const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


// Alternative ways to retrieve elements

// document.getElementById('txt-first-name');
// document.getElementByClassName('txt-inputs');
// document.getElementByTagName('input');


let fVal = txtFirstName.value;
let lVal = txtLastName.value;


txtFirstName.addEventListener('keyup', function() {
    displayFullName(fVal, lVal);
});


txtLastName.addEventListener('keyup', function() {
    displayFullName(fVal, lVal);
});


function displayFullName (fVal, lVal) {
	spanFullName.innerHTML = (txtFirstName.value + " " + txtLastName.value);
};

